from jace import Chess
from jace.classes import PlayerTurn, PlayerTurnFlag
from jace.consts import FEN_START  # noqa

global moves
chess = Chess()
# result = chess.init_game_fen('rnb1kbnr/ppp1pppp/8/3P4/1q6/7P/PPPP1PP1/RNBQKBNR w KQkq - 1 4')  # noqa
result = chess.init_game_fen(FEN_START)  # noqa
moves = result.moves

try:
    ending = False
    while not ending:

        valid = False
        while not valid:
            print(chess)
            print(f'{chess.curr_player} to move ')
            print(f'valid moves= {moves}')

            f = input('From: ')
            t = input('To: ')

            result: PlayerTurn = chess.try_move(int(f.strip()), int(t.strip()))
            valid = result.valid
            if valid:
                moves = result.moves
            if result.flag:
                if result.flag == PlayerTurnFlag.CHECKMATE:
                    print(f'{chess.curr_player} Wins!')
                    exit(0)
                print(result.flag)
except KeyboardInterrupt:
    exit(0)
