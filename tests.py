from jace.bitboard import Chess
from jace.utils import mask_file, mask_rank, bb_from_fieldname
from jace.consts import FEN_START, Ranks, Files, Colors, Pieces, MSB_BB
from jace.serializer import (
    engine_dumps, engine_loads,
    movecache_dumps, movecache_loads
)
from jace.movecache import MoveCache

import pytest
from numpy import uint64 as u64


MC = MoveCache()


def test_bb_from_fieldname_invalid():
    with pytest.raises(KeyError):
        bb_from_fieldname('a9')
    with pytest.raises(KeyError):
        bb_from_fieldname('i2')


def test_bb_from_fieldname_valid():
    assert bb_from_fieldname('a5') == u64(0x0000000100000000)
    assert bb_from_fieldname('h8') == MSB_BB


def test_masks():
    assert mask_rank(Ranks.FIVE) == u64(1095216660480)
    assert ~mask_rank(Ranks.FIVE) == u64(18446742978492891135)
    assert mask_file(Files.A) == u64(72340172838076673)
    assert ~mask_file(Files.A) == u64(18374403900871474942)


def test_chess_repr():
    chess = Chess(mc_obj=MC)
    chess.pieces[Colors.WHITE][Pieces.PAWN] = u64(0x000000000000FF00)
    chess.pieces[Colors.WHITE][Pieces.KNIGHT] = u64(0x0000000000000042)
    chess.pieces[Colors.WHITE][Pieces.BISHOP] = u64(0x0000000000000024)
    chess.pieces[Colors.WHITE][Pieces.ROOK] = u64(0x0000000000000081)
    chess.pieces[Colors.WHITE][Pieces.QUEEN] = u64(0x0000000000000008)
    chess.pieces[Colors.WHITE][Pieces.KING] = u64(0x0000000000000010)

    for wpieces in chess.pieces[Colors.WHITE]:
        chess.white_pieces |= wpieces

    chess.pieces[Colors.BLACK][Pieces.PAWN] = u64(0x00FF000000000000)
    chess.pieces[Colors.BLACK][Pieces.KNIGHT] = u64(0x4200000000000000)
    chess.pieces[Colors.BLACK][Pieces.BISHOP] = u64(0x2400000000000000)
    chess.pieces[Colors.BLACK][Pieces.ROOK] = u64(0x8100000000000000)
    chess.pieces[Colors.BLACK][Pieces.QUEEN] = u64(0x0800000000000000)
    chess.pieces[Colors.BLACK][Pieces.KING] = u64(0x1000000000000000)

    for bpieces in chess.pieces[Colors.BLACK]:
        chess.black_pieces |= bpieces
    chess.all_pieces = chess.black_pieces | chess.white_pieces
    x = Chess(mc_obj=MC)
    x.init_game_fen('rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1')
    assert x.__repr__() == chess.__repr__()
    assert x.all_pieces == chess.all_pieces
    assert x.black_pieces == chess.black_pieces
    assert x.white_pieces == chess.white_pieces


def test_serialization():
    chess = Chess(mc_obj=MC)
    chess.init_game_fen('r1b2b1r/8/8/8/8/8/8/R1B2B1R w - - 0 1')
    chess.enpassant[Colors.WHITE] = u64(512)
    blob = engine_dumps(chess)
    mcblob = movecache_dumps(chess.mc)
    other = engine_loads(blob)
    other.mc = movecache_loads(mcblob)
    assert chess.legal_movegen() == other.legal_movegen()
    assert chess.white_pieces == other.white_pieces
    assert chess.black_pieces == other.black_pieces
    assert chess.next_player == other.next_player
    assert chess.fen.generate() == other.fen.generate()
    assert chess.enpassant == other.enpassant


def test_not_castle_twice_white():
    chess = Chess(mc_obj=MC)
    chess.init_game_fen(
        'rnbqkbnr/pppppppp/8/8/8/8/PPPPPP1P/RNBQK2R w KQkq - 0 1'
    )
    chess.try_move(4, 6)
    chess.curr_player = chess.next_player
    chess.try_move(6, 14)
    chess.curr_player = chess.next_player
    chess.try_move(5, 7)
    chess.curr_player = chess.next_player
    chess.try_move(14, 5)
    chess.curr_player = chess.next_player
    chess.try_move(5, 4)
    chess.curr_player = chess.next_player
    x = chess.legal_movegen()
    assert x[4] == [5]


def test_remember_castling():
    chess = Chess(mc_obj=MC)
    chess.init_game_fen(
        'rnbqkbnr/pppppppp/8/8/8/8/PPPPPP1P/RNBQK2R w KQkq - 0 1'
    )
    chess.try_move(4, 6)
    z = engine_loads(engine_dumps(chess))
    z.mc = MC
    z.try_move(48, 40)
    assert z.fen.castling == 'kq'


def test_pawn_moves_no_crossing_sides():
    chess = Chess(mc_obj=MC)
    chess.init_game_fen('k7/8/8/8/p7/8/PP5P/RNBQKBNR w KQkq - 0 1')
    next_turn = chess.try_move(9, 25)
    assert next_turn.moves[24] == [16, 17]


def test_enpassant():
    chess = Chess(mc_obj=MC)
    chess.init_game_fen('k7/7p/8/8/p7/8/PP4PP/RNBQKBNR w KQkq - 0 1')
    result = chess.try_move(9, 25)
    assert result.valid is True
    assert chess.enpassant[Colors.WHITE] == 33554432
    assert result.moves[24] == [16, 17]
    assert result.moves[55] == [39, 47]
    result = chess.try_move(24, 17)
    assert result.valid is True
    assert chess.pieces[Colors.WHITE][Pieces.PAWN] == 49408


def test_enpassant_not_across_board():
    chess = Chess(mc_obj=MC)
    chess.init_game_fen(FEN_START)
    nt = chess.try_move(8, 24)
    assert nt.moves[48] == [32, 40]
