from .movecache import MoveCache
from .bitboard import Chess
from .consts import Colors, ROOK_MAGIC, BISHOP_MAGIC
from .classes import FenString
import orjson
import numpy as np
from numpy import uint64 as u64


def engine_dumps(engine: Chess) -> dict:
    return orjson.dumps({
        'curr_player': engine.curr_player,
        'pieces': engine.pieces,
        'enpassant_white': engine.enpassant[Colors.WHITE],
        'enpassant_black': engine.enpassant[Colors.BLACK],
        'fen': engine.fen
    }, option=orjson.OPT_SERIALIZE_NUMPY)


def engine_loads(blob: bytes) -> Chess:
    blob = orjson.loads(blob)
    engine = Chess(False)
    engine.enpassant[Colors.WHITE] = blob['enpassant_white']
    engine.enpassant[Colors.BLACK] = blob['enpassant_black']
    engine.curr_player = Colors(blob['curr_player'])
    engine.pieces = np.array(blob['pieces'], dtype=u64)
    # this allows for dumping the engine before a match is initialized
    if 'fen' in blob and blob['fen']:
        engine.fen = FenString(**blob['fen'])
        if 'KQ' not in engine.fen.castling:
            engine.moved_king[Colors.WHITE] = True
        if 'kq' not in engine.fen.castling:
            engine.moved_king[Colors.BLACK] = True
    engine._update_pieces()
    return engine


def movecache_dumps(mc: MoveCache) -> dict:
    return orjson.dumps({
        'ROOK_ATT': mc.ROOK_ATT,
        'ROOK_OFFSET': mc.ROOK_OFFSET,
        'ROOK_MASK': mc.ROOK_MASK,
        'BISHOP_ATT': mc.BISHOP_ATT,
        'BISHOP_OFFSET': mc.BISHOP_OFFSET,
        'BISHOP_MASK': mc.BISHOP_MASK,
        'KING': mc.KING,
        'KNIGHT': mc.KNIGHT
    }, option=orjson.OPT_SERIALIZE_NUMPY)


def movecache_loads(blob: dict) -> MoveCache:
    if not isinstance(blob, dict):
        blob = orjson.loads(blob)

    mc = MoveCache(skip_init=True)
    mc.ROOK_ATT = np.array(blob['ROOK_ATT'], dtype=u64)
    mc.ROOK_OFFSET = np.array(blob['ROOK_OFFSET'], dtype=u64)
    mc.ROOK_MASK = np.array(blob['ROOK_MASK'], dtype=u64)
    mc.BISHOP_ATT = np.array(blob['BISHOP_ATT'], dtype=u64)
    mc.BISHOP_OFFSET = np.array(blob['BISHOP_OFFSET'], dtype=u64)
    mc.BISHOP_MASK = np.array(blob['BISHOP_MASK'], dtype=u64)
    mc.KING = np.array(blob['KING'], dtype=u64)
    mc.KNIGHT = np.array(blob['KNIGHT'], dtype=u64)
    mc.ROOK_MAGIC = ROOK_MAGIC
    mc.BISHOP_MAGIC = BISHOP_MAGIC

    return mc
