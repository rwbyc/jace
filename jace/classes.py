from .consts import Pieces, Colors
from .utils import iter_bb_indexes

import numpy as np
from dataclasses import dataclass
from numpy import ndarray, uint64 as u64, uint8 as u8
from functools import lru_cache
from enum import IntEnum
from typing import Union, List


@dataclass
class BBSearchResult:
    piece: Pieces
    color: Colors
    mask: u64


@dataclass
class BBPieceSearchResult:
    piece: Pieces
    bb: u64
    fieldname: str


@dataclass
class MoveResult:
    valid: bool
    takes: Union[bool, None]
    source: Union[BBPieceSearchResult, None]
    dest: Union[BBPieceSearchResult, None]


class PlayerTurnFlag(IntEnum):
    NONE = 0
    CHECK = 1
    CHECKMATE = 2


@dataclass
class PlayerTurn:
    valid: bool
    moves: Union[dict, None]
    fen: str
    flag: PlayerTurnFlag = PlayerTurnFlag.NONE


@dataclass
class PseudoMove:
    piece: Pieces
    piece_bb: u64
    attack_bb: List[u64]


@dataclass
class FenString:
    # TODO check strings on creation e.g. board contains 7*/
    board: str
    side: str
    castling: str
    enp: str
    hm: str
    fm: str

    def generate(self) -> str:
        return f'{self.board} {self.side} {self.castling} {self.enp} {self.hm} {self.fm}'  # noqa

    def set_side(self, color: Colors) -> None:
        self.side = color.get_char()

    def set_castling(self, has_castled: dict) -> None:
        self.castling = 'KQ' if not has_castled[Colors.WHITE] else ''
        self.castling += 'kq' if not has_castled[Colors.BLACK] else ''
        self.castling = '-' if self.castling == '' else self.castling

    def get_side(self) -> Colors:
        return Colors(0) if self.side.lower() == 'w' else Colors(1)

    def set_board(self, pieces: ndarray) -> None:
        boardstr = ['.'] * 64
        for color in Colors:
            for piece in Pieces:
                bb = pieces[color][piece]
                for index in iter_bb_indexes(bb):
                    boardstr[index] = piece.get_char(color)

        fen = part = ''
        char_count = dot_count = 0
        for char in boardstr:
            if char == '.':
                dot_count += 1
            else:
                if dot_count != 0:
                    part += str(dot_count)
                part += char
                dot_count = 0
            char_count += 1

            if char_count == 8:    
                if dot_count != 0:
                    part += str(dot_count)

                fen = f'{part}/{fen}'
                part = ''
                char_count = 0
                dot_count = 0

        self.board = fen[:-1]


class Directions(IntEnum):
    NORTH = 0,
    SOUTH = 1,
    EAST = 2,
    WEST = 3,
    NORTH_WEST = 4,
    NORTH_EAST = 5,
    SOUTH_WEST = 6,
    SOUTH_EAST = 7

    @staticmethod
    def rook():
        return [
            Directions.NORTH,
            Directions.SOUTH,
            Directions.EAST,
            Directions.WEST,
        ]

    @staticmethod
    def bishop():
        return [
            Directions.NORTH_WEST,
            Directions.NORTH_EAST,
            Directions.SOUTH_WEST,
            Directions.SOUTH_EAST,
        ]

    @lru_cache(maxsize=256)
    def edge_distance(self, index: int) -> u8:
        """ returns the distance to the edge of the board
        excluding the current index square

        :param index: index of the square
        :type index: int
        :return: distance to edge for given Direction
        :rtype: int
        """
        row = u8(index / 8)
        col = u8(index % 8)
        x = u8(0)

        def _inv(x: u8) -> u8:
            return np.subtract(7, x, dtype=u8)

        if   (self == self.NORTH): x = _inv(row)  # noqa
        elif (self == self.SOUTH): x = row  # noqa
        elif (self == self.EAST): x = _inv(col)  # noqa
        elif (self == self.WEST): x = col  # noqa
        elif (self == self.NORTH_EAST): x = min(_inv(row), _inv(col))  # noqa
        elif (self == self.NORTH_WEST): x = min(_inv(row), col)  # noqa
        elif (self == self.SOUTH_EAST): x = min(row, _inv(col))  # noqa
        elif (self == self.SOUTH_WEST): x = min(row, col)  # noqa

        assert x >= 0 and x <= 7
        return x

    @lru_cache(maxsize=256)
    def next_index(self, index: u8) -> u8:
        row = int(index / 8)
        col = int(index % 8)

        if (self == Directions.NORTH): row += 1  # noqa
        elif (self == Directions.SOUTH): row -= 1  # noqa
        elif (self == Directions.EAST): col += 1  # noqa
        elif (self == Directions.WEST): col -= 1  # noqa
        elif (self == Directions.NORTH_EAST): row += 1;col += 1  # noqa
        elif (self == Directions.NORTH_WEST): row += 1;col -= 1  # noqa
        elif (self == Directions.SOUTH_EAST): row -= 1;col += 1  # noqa
        elif (self == Directions.SOUTH_WEST): row -= 1;col -= 1  # noqa

        rv = row * 8 + col
        if (row > 7 or col > 7 or row < 0 or col < 0):
            return u8(255)
        return u8(rv)
