from .utils import (
    mask_file, mask_rank
)

from .classes import Directions
from .consts import (
    FULL_BB,
    LSB_BB,
    LSB_BB8,
    Ranks,
    Files,
    ROOK_MAGIC,
    BISHOP_MAGIC,
    Colors
)

from numpy import uint64 as u64, uint8 as u8
import numpy as np
from typing import Union, List, Optional, Dict
from functools import lru_cache


def get_occupancies_from_direction(
        direction: Directions, index: u8
        ) -> Union[None, List[u64]]:
    distance = np.subtract(
        direction.edge_distance(index), 1, dtype=u8
    )
    if distance == 255:
        return None

    possible_occupancies = (u8(1) << distance)
    bb_rv = []

    occupancy = u8(0)
    while occupancy < possible_occupancies:
        bitboard = u64(0)
        mask = LSB_BB8
        next_index = index

        while mask <= occupancy:
            next_index = direction.next_index(next_index)
            assert index != 255
            #: fyi: `not not` instead of bool would also work here.
            #: We only want the result of the & operation in 0 or 1
            #: that's why there is the u64
            #: e.g. 5 & 4 == 4 but we only want 0 or 1 as result
            #: int(bool(5 & 4)) == 1
            bitboard |= u64(bool(occupancy & mask)) << u8(next_index)
            mask <<= u8(1)

        bb_rv.append(bitboard)
        occupancy = np.add(occupancy, 1, dtype=u8)
    return bb_rv


def generate_all_occupancies(
        directions: List[Directions], loop_index: int
        ) -> Optional[np.ndarray]:
    occupancies = []
    for direction in directions:
        if bb_rv := get_occupancies_from_direction(direction, loop_index):
            if not len(occupancies):
                occupancies = np.array(bb_rv, dtype=u64)
                continue

            occupancies = np.array(
                [(bb | occ) for bb in bb_rv for occ in occupancies], dtype=u64
            )
    return occupancies


@lru_cache(maxsize=256)
def _next_index_mask(next_index: u8) -> u64:
    return (LSB_BB << u64(next_index))


def generate_attack(
        loop_index: u8,
        occupancy: u64,
        IM
        ) -> u64:
    attack_bb = u64(0)
    for direction in IM[loop_index]:
        for next_index in IM[loop_index][direction]:
            mask = _next_index_mask(next_index)
            attack_bb |= mask
            if occupancy & mask:
                break
    return attack_bb


def calc_att_table(
        index, occupancies, attacks, shift_bits, is_rook
            ) -> np.ndarray:
    table = np.zeros((LSB_BB << shift_bits), dtype=u64)
    magic = ROOK_MAGIC[index]
    if not is_rook:
        magic = BISHOP_MAGIC[index]

    for i in range(len(occupancies)):
        occ = occupancies[i]
        att = attacks[i]

        offset = np.multiply(occ, magic) >> (u64(64) - shift_bits)
        table[offset] = att
    return table


def calc_att_table_force(occupancies, attacks, shift_bits):
    while 1:
        table = [FULL_BB] * (LSB_BB << shift_bits)
        magic = u64(612498416294952992)
        print(magic)
        collision = False

        for i in range(len(occupancies)):
            occ = occupancies[i]
            att = attacks[i]
            offset = np.multiply(occ, magic) >> (u64(64) - shift_bits)

            if (table[offset] == FULL_BB or table[offset] == att):
                table[offset] = att
            else:
                collision = True
                break

        if not collision:
            print(f'found magic={magic}')
            return magic, table


def compute_king_move(king_bb: u64) -> u64:
    clip_file_h = king_bb & ~mask_file(Files.H)
    clip_file_a = king_bb & ~mask_file(Files.A)

    nw = clip_file_a << u8(7)
    n = king_bb << u8(8)
    ne = clip_file_h << u8(9)
    e = clip_file_h << u8(1)
    se = clip_file_h >> u8(7)
    s = king_bb >> u8(8)
    sw = clip_file_a >> u8(9)
    w = clip_file_a >> u8(1)

    king_moves = nw | n | ne | e | se | s | sw | w
    # return king_moves & ~side_bb
    return king_moves


def compute_knight_move(knight_bb: u64) -> u64:
    clip_1 = clip_8 = ~mask_file(Files.A) & ~mask_file(Files.B)
    clip_2 = clip_7 = ~mask_file(Files.A)
    clip_3 = clip_6 = ~mask_file(Files.H)
    clip_4 = clip_5 = ~mask_file(Files.G) & ~mask_file(Files.H)

    spot_1 = (knight_bb & clip_1) << u8(6)
    spot_2 = (knight_bb & clip_2) << u8(15)
    spot_3 = (knight_bb & clip_3) << u8(17)
    spot_4 = (knight_bb & clip_4) << u8(10)
    spot_5 = (knight_bb & clip_5) >> u8(6)
    spot_6 = (knight_bb & clip_6) >> u8(15)
    spot_7 = (knight_bb & clip_7) >> u8(17)
    spot_8 = (knight_bb & clip_8) >> u8(10)

    knight_moves = spot_1 | spot_2 | spot_3 | spot_4 | spot_5 |\
        spot_6 | spot_7 | spot_8
    # return knight_moves & ~side_bb
    return knight_moves


def compute_pawn_white_move(
        pawns_bb: u64, all_pieces: u64, black_pieces: u64,
        enpassant: Dict[Colors, u64]
) -> u64:
    pawns_step_one = (pawns_bb << u8(8)) & ~all_pieces
    pawns_step_two = ((pawns_step_one & mask_rank(Ranks.THREE)) <<
                      u8(8)) & ~all_pieces
    pawns_moves = pawns_step_one | pawns_step_two

    pawns_attack_left = (pawns_bb << u64(7)) & ~mask_file(Files.H)
    pawns_attack_right = (pawns_bb << u64(9)) & ~mask_file(Files.A)
    pawns_possible_attacks = pawns_attack_left | pawns_attack_right

    pawns_attacks = pawns_possible_attacks & (
        black_pieces | u64(enpassant[Colors.BLACK] << u8(8))
    )
    return pawns_attacks | pawns_moves


def compute_pawn_black_move(
        pawns_bb: u64, all_pieces: u64, white_pieces: u64,
        enpassant: Dict[Colors, u64]
) -> u64:
    pawns_step_one = (pawns_bb >> u8(8)) & ~all_pieces
    pawns_step_two = ((pawns_step_one & mask_rank(Ranks.SIX)) >>
                      u8(8)) & ~all_pieces
    pawns_moves = pawns_step_one | pawns_step_two

    pawns_attack_left = (pawns_bb >> u8(7)) & ~mask_file(Files.A)
    pawns_attack_right = (pawns_bb >> u8(9)) & ~mask_file(Files.H)
    pawns_possible_attacks = pawns_attack_left | pawns_attack_right

    pawns_attacks = pawns_possible_attacks & (
        white_pieces | u64(enpassant[Colors.WHITE] >> u8(8))
    )
    return pawns_attacks | pawns_moves


from random import getrandbits  # noqa
def random_u64_zero_biased():  # noqa
    def _inner():
        return (u64(0xFFFF & getrandbits(64)) << u64(48)) | \
            (u64(0xFFFF & getrandbits(64)) << u64(32)) | \
            (u64(0xFFFF & getrandbits(64)) << u64(16)) | \
            u64(0xFFFF & getrandbits(64))

    return _inner() & _inner() & _inner()

# def random_u64():
#     x = np.random.randint(
#             1,
#             FULL_BB-1, size=3, dtype=np.uint64)
#     return x[0] & x[1] & x[2]


# def force_magic(index, occupancies, attacks, shift_bits, is_rook):
#     assert len(occupancies) == len(attacks)

#     while True:
#         table = [FULL_BB] * (LSB_BB << shift_bits)

#         magic = random_u64_zero_biased()
#         collision = False

#         for i in np.arange(len(occupancies)):
#             occ = occupancies[i]
#             att = attacks[i]
#             offset = np.multiply(occ, magic) >> (u64(64) - shift_bits)

#             assert offset > -1

#             if (table[offset] == FULL_BB or table[offset] == att):
#             else:
#                 collision = True
#                 table[offset] = att
#                 break

#         if not collision:
#             return magic, table
