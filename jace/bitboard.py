from .movegen import compute_pawn_white_move, compute_pawn_black_move
from .movecache import MoveCache
from .utils import (  # noqa
    bb_from_fieldname,
    index_to_fieldname,
    lsb_bitscan,
    print_u64_board,
    iter_bb,
    iter_bb_indexes,
    fieldname_to_index,
    mask_rank,
    mask_file
)
from .classes import (
    BBSearchResult, FenString,
    BBPieceSearchResult, MoveResult, PlayerTurn, PlayerTurnFlag,
    PseudoMove
)
from .consts import (
    Colors, Pieces, ROOK_SHIFTS, BISHOP_SHIFTS,
    EMPTY_BB, MSB_BB, LSB_BB, CASTLE_MASKS, Ranks
)

import numpy as np
from numpy import uint64 as u64, uint8 as u8
from typing import Union, List


class Chess:
    def __init__(self, mc_obj=None):
        self.pieces = np.zeros((2, 6), dtype=np.uint64)
        self.white_pieces = u64(0)
        self.black_pieces = u64(0)
        self.all_pieces = u64(0)
        self.fen = None
        self.mc = mc_obj if mc_obj is not None else MoveCache()
        self.enpassant = {
            Colors.WHITE: u64(0),
            Colors.BLACK: u64(0)
        }

        self.curr_player = Colors.WHITE
        self.moved_king = {
            Colors.WHITE: False,
            Colors.BLACK: False
        }

    @property
    def next_player(self):
        return Colors(not self.curr_player)

    def _copy(self):
        copy = Chess(False)
        copy.mc = self.mc  #: this way the MoveCache does not initialize again
        copy.pieces = np.copy(self.pieces)
        copy.curr_player = self.curr_player
        copy.moved_king = self.moved_king
        copy.enpassant = self.enpassant
        copy._update_pieces()
        return copy

    def _insert(self, other) -> None:
        self.pieces = np.copy(other.pieces)
        self.moved_king = other.moved_king
        self.enpassant = other.enpassant
        self._update_pieces()

    def __repr__(self) -> str:
        out = line = ''
        ptr = u64(1)
        for index in np.arange(64, dtype=u64):
            #: ptr << index generates a bb mask where the 1 shifts
            #: to the left with every iteration
            #: e.g for 8bit: index=0 == 00000001; index=1 == 00000010 etc.
            if result := self.search_piece_by_mask(ptr << index):
                line += f'{result.piece.get_char(result.color)}'
            else:
                line += '.'

            if (index+1) % 8 == 0:
                out = f'{int(index/8) + 1} {line}\n{out}'
                line = ''

        return f'{out}\n  ABCDEFGH\n'

    def _update_pieces(self) -> None:
        self.all_pieces = u64(0)
        self.black_pieces = u64(0)
        self.white_pieces = u64(0)

        for wpieces in self.pieces[Colors.WHITE]:
            self.white_pieces |= wpieces

        for bpieces in self.pieces[Colors.BLACK]:
            self.black_pieces |= bpieces
        self.all_pieces = self.black_pieces | self.white_pieces

    def get_rook_attacktable_index(self, occ: u64, index: u8) -> u64:
        """
        :param occ: occupancy bitboard of all pieces on the board
        :type occ: np.uint64
        :param index: square index
        :type index: int | np.uint64
        :return: the returned value is used as index for the attack table
            the result still needs to remove all of the player side pieces
            e.g. ~white_pieces & attack_table[return_value]
        """
        occ_mask = u64(self.mc.ROOK_MASK[index] & occ)
        magic = u64(self.mc.ROOK_MAGIC[index])
        shifts = u64(ROOK_SHIFTS[index])
        offset = u64(self.mc.ROOK_OFFSET[index])
        return ((np.multiply(occ_mask, magic) >> (u64(64) - shifts)) + offset)

    def get_bishop_attacktable_index(self, occ: u64, index: u8) -> u64:
        """
        :param occ: occupancy bitboard of all pieces on the board
        :type occ: np.uint64
        :param index: square index
        :type index: int | np.uint64 | np.uint8
        :return: the returned value is used as index for the attack table
            the result still needs to remove all of the player side pieces
            e.g. ~white_pieces & attack_table[return_value]
        """
        occ_mask = u64(self.mc.BISHOP_MASK[index] & occ)
        magic = u64(self.mc.BISHOP_MAGIC[index])
        shifts = u64(BISHOP_SHIFTS[index])
        offset = u64(self.mc.BISHOP_OFFSET[index])
        return ((np.multiply(occ_mask, magic) >> (u64(64) - shifts)) + offset)

    def get_pseudo_rook_moves(
            self, piece_index: int) -> Union[PseudoMove, None]:
        att_index = self.get_rook_attacktable_index(
            self.all_pieces, piece_index
        )

        pseudo_attacks = ~self.my_pieces() & self.mc.ROOK_ATT[att_index]
        if pseudo_attacks != EMPTY_BB:
            return PseudoMove(
                Pieces.ROOK, (u64(1) << piece_index), pseudo_attacks
            )
        return None

    def get_pseudo_bishop_moves(
            self, piece_index: int) -> Union[PseudoMove, None]:
        att_index = self.get_bishop_attacktable_index(
            self.all_pieces, piece_index
        )
        pseudo_attacks = ~self.my_pieces() & self.mc.BISHOP_ATT[att_index]
        if pseudo_attacks != EMPTY_BB:
            return PseudoMove(
                Pieces.BISHOP, (u64(1) << piece_index), pseudo_attacks
            )
        return None

    def get_pseudo_king_moves(
            self, *args, **kwargs) -> Union[PseudoMove, None]:
        king_bb = self.pieces[self.curr_player][Pieces.KING]
        # this shouldn't happen in a real game, but is needed for testing
        if king_bb == EMPTY_BB:
            return None
        king_index = lsb_bitscan(king_bb)
        pseudo_attacks = self.mc.KING[king_index]
        pseudo_attacks &= ~self.my_pieces()

        if pseudo_attacks != EMPTY_BB:
            return PseudoMove(Pieces.KING, king_bb, pseudo_attacks)
        return None

    def get_pseudo_knight_moves(
            self, piece_index: int) -> Union[PseudoMove, None]:
        pseudo_attacks = self.mc.KNIGHT[piece_index]
        pseudo_attacks &= ~self.my_pieces()

        if pseudo_attacks != EMPTY_BB:
            return PseudoMove(
                Pieces.KNIGHT, (u64(1) << piece_index), pseudo_attacks
            )
        return None

    def get_pseudo_pawn_moves(
            self, piece_index: int
            ) -> Union[PseudoMove, None]:
        pawn_bb = (u64(1) << piece_index)

        func = compute_pawn_white_move if self.curr_player == Colors.WHITE else compute_pawn_black_move # noqa
        pseudo_attacks = func(
            pawn_bb, self.all_pieces, self.other_pieces(), self.enpassant
        )
        if pseudo_attacks != EMPTY_BB:
            return PseudoMove(Pieces.PAWN, pawn_bb, pseudo_attacks)
        return None

    def get_pseudo_queen_moves(self, queen_index: u64):
        moves = PseudoMove(Pieces.QUEEN, (u64(1) << queen_index), u64(0))

        if pseudo := self.get_pseudo_bishop_moves(queen_index):
            moves.attack_bb |= pseudo.attack_bb

        if pseudo := self.get_pseudo_rook_moves(queen_index):
            moves.attack_bb |= pseudo.attack_bb

        if moves.attack_bb != EMPTY_BB:
            return moves
        return None

    def search_piece_by_mask(self, mask: u64) -> Union[BBSearchResult, None]:
        for ecolor in Colors:
            for epiece in Pieces:
                result = self.pieces[ecolor][epiece] & mask != EMPTY_BB
                if result:
                    return BBSearchResult(epiece, ecolor, mask)
        return None

    def search_piece_by_fieldname(
            self, fieldname: str, color: Colors
            ) -> Union[BBPieceSearchResult, None]:

        source_bb = bb_from_fieldname(fieldname)
        for piece in Pieces:
            pieces_bb = self.pieces[color][piece]
            for bb in iter_bb(pieces_bb):
                if (bb ^ source_bb) == 0:
                    return BBPieceSearchResult(piece, bb, fieldname)
        return None

    def init_game_fen(self, fen_str: str) -> dict:
        self.fen = FenString(*fen_str.split(' '))

        mask = MSB_BB
        for rank in self.fen.board.split('/'):
            for char in rank[::-1]:
                #: shift at least on bit to the right at the end
                #: if char is a number shift by that amount instead
                shift = LSB_BB
                if char.isnumeric():
                    shift = u64(char)
                else:
                    piece = Pieces.create_from_char(char)
                    color = Colors.BLACK if char.islower() else Colors.WHITE

                    self.pieces[color][piece] |= mask
                mask = mask >> shift

        self.curr_player = self.fen.get_side()
        self._update_pieces()
        return PlayerTurn(True, self.legal_movegen(), fen_str)

    def my_pieces(self, color: Colors = None) -> u64:
        if color is None:
            color = self.curr_player
        return \
            self.white_pieces if color == Colors.WHITE else self.black_pieces

    def other_pieces(self, color: Colors = None) -> u64:
        if color is None:
            color = self.curr_player
        return \
            self.white_pieces if color == Colors.BLACK else self.black_pieces

    def pseudo_movegen_piece(
            self, piece: Pieces, piece_index: int) -> Union[PseudoMove, None]:
        func_map = {
            Pieces.PAWN: self.get_pseudo_pawn_moves,
            Pieces.QUEEN: self.get_pseudo_queen_moves,
            Pieces.KNIGHT: self.get_pseudo_knight_moves,
            Pieces.BISHOP: self.get_pseudo_bishop_moves,
            Pieces.ROOK: self.get_pseudo_rook_moves,
            Pieces.KING: self.get_pseudo_king_moves
        }
        return func_map[piece](piece_index)

    def pseudo_movegen(self) -> List[PseudoMove]:
        MOVES = []
        for piece in Pieces:
            for index in iter_bb_indexes(self.pieces[self.curr_player][piece]):
                if moves := self.pseudo_movegen_piece(piece, index):
                    MOVES.append(moves)
        return MOVES

    def not_king_has_moved(self) -> bool:
        if self.curr_player == Colors.WHITE and not self.moved_king[Colors.WHITE]:  # noqa
            return bool(self.pieces[self.curr_player][Pieces.KING] & u64(0x10))
        elif self.curr_player == Colors.BLACK and not self.moved_king[Colors.BLACK]:  # noqa
            return bool(self.pieces[self.curr_player][Pieces.KING] & u64(
                    0x1000000000000000))
        return False

    def add_castling_moves(self) -> u64:
        moves = u64(0)
        if not self.not_king_has_moved():
            return moves

        if self.curr_player == Colors.WHITE:
            if (not bool(self.my_pieces() & CASTLE_MASKS.WHITE_QUEEN)) and (self.pieces[self.curr_player][Pieces.ROOK] & u64(0x80)):  # noqa
                moves |= u64(0x4)
            if (not bool(self.my_pieces() & CASTLE_MASKS.WHITE_KING)) and (self.pieces[self.curr_player][Pieces.ROOK] & u64(0x1)):  # noqa
                moves |= u64(0x40)
        elif self.curr_player == Colors.BLACK:
            if (not bool(self.my_pieces() & CASTLE_MASKS.BLACK_QUEEN)) and (self.pieces[self.curr_player][Pieces.ROOK] & u64(0x8000000000000000)):  # noqa
                moves |= u64(0x0400000000000000)
            if (not bool(self.my_pieces() & CASTLE_MASKS.BLACK_KING)) and (self.pieces[self.curr_player][Pieces.ROOK] & u64(0x0100000000000000)):  # noqa
                moves |= u64(0x4000000000000000)
        return moves

    def register_enpassant(
        self, source_piece: Pieces, source_bb: u64, dest_bb: u64
    ) -> None:
        dest_mask = (source_bb << u8(16)) if self.curr_player == Colors.WHITE else (source_bb >> u8(16))  # noqa
        rank = Ranks.TWO if self.curr_player == Colors.WHITE else Ranks.SEVEN

        self.enpassant[self.curr_player] = u64(0)
        if (
            source_bb & mask_rank(rank) and
            bool((dest_mask & dest_bb)) and
            source_piece == Pieces.PAWN
        ):
            self.enpassant[self.curr_player] = dest_bb

    def apply_enpassant_attack_bb(self, pawn_bb: u64) -> int:
        if self.enpassant[self.next_player]:
            if self.next_player == Colors.WHITE:
                pseudo_moves = compute_pawn_black_move(
                    pawn_bb, self.all_pieces,
                    self.white_pieces, self.enpassant
                )
                enpass_bb = u64(self.enpassant[self.next_player] >> u8(8))
                if pseudo_moves & enpass_bb:
                    return enpass_bb
            elif self.next_player == Colors.BLACK:
                pseudo_moves = compute_pawn_white_move(
                    pawn_bb, self.all_pieces,
                    self.black_pieces, self.enpassant
                )
                enpass_bb = u64(self.enpassant[self.next_player] << u8(8))
                if pseudo_moves & enpass_bb:
                    return enpass_bb
        return 0

    def legal_movegen(self) -> dict:
        moves = self.pseudo_movegen()

        for pseudo in moves:
            if pseudo.piece == Pieces.PAWN:
                pseudo.attack_bb |= u64(
                    self.apply_enpassant_attack_bb(pseudo.piece_bb)
                )
            for move_bb in iter_bb(pseudo.attack_bb):
                bc = self._copy()

                bc.pieces[bc.curr_player][pseudo.piece] ^= pseudo.piece_bb
                bc.pieces[bc.curr_player][pseudo.piece] |= move_bb  # noqa
                bc._update_pieces()

                # see if by applying the pseudo move we leave our king in
                # check
                # returns check bb where each set bit is a sq t
                check_bb = bc.is_king_in_check(bc.next_player)

                if check_bb:
                    if check_bb & move_bb:
                        continue
                    pseudo.attack_bb ^= move_bb

            if pseudo.piece == Pieces.KING:
                pseudo.attack_bb |= self.add_castling_moves()

        return {
            int(lsb_bitscan(x.piece_bb)): (lambda v: [int(x) for x in iter_bb_indexes(v)])(x.attack_bb) for x in moves if x.attack_bb != EMPTY_BB  # noqa
        }

    def apply_move_castling(self, source_bb: u64, dest_bb: u64):
        dest_col = int(lsb_bitscan(dest_bb) % 8)
        if dest_col == 6:  # king side
            rook_bb = (self.pieces[self.curr_player][Pieces.ROOK] & u64(0x8000000000000080))  # noqa
            if rook_bb == EMPTY_BB:
                return
            self.pieces[self.curr_player][Pieces.ROOK] ^= rook_bb
            self.pieces[self.curr_player][Pieces.ROOK] |= (rook_bb >> u64(2))
        elif dest_col == 2:  # queen side
            rook_bb = (self.pieces[self.curr_player][Pieces.ROOK] & u64(0x0100000000000001))  # noqa
            if rook_bb == EMPTY_BB:
                return
            self.pieces[self.curr_player][Pieces.ROOK] ^= rook_bb
            self.pieces[self.curr_player][Pieces.ROOK] |= (rook_bb << u64(3))

    def apply_move(
        self, source_bb: u64, source_piece: Pieces, dest_bb: u64,
        dest_piece: Union[Pieces, None] = None,
        promotion: Pieces = Pieces.QUEEN
    ) -> None:
        # take enemy piece by removing it from the pieces bb
        if dest_piece is not None:
            bb = self.pieces[self.next_player][dest_piece]
            self.pieces[self.next_player][dest_piece] = (bb ^ dest_bb)
        # check if the player took a piece via en passant
        if source_piece == Pieces.PAWN and self.enpassant[self.next_player]:
            # possible taken pawn
            pawn_bb = u64(self.enpassant[self.next_player]) & self.pieces[self.next_player][Pieces.PAWN]  # noqa
            if self.next_player == Colors.WHITE:
                if bool((pawn_bb >> u8(8)) & dest_bb):
                    self.pieces[self.next_player][Pieces.PAWN] ^= pawn_bb
            elif self.next_player == Colors.BLACK:
                if bool((pawn_bb << u8(8)) & dest_bb):
                    self.pieces[self.next_player][Pieces.PAWN] ^= pawn_bb

        if source_piece == Pieces.KING:
            if self.not_king_has_moved():
                self.apply_move_castling(source_bb, dest_bb)
            # self.moved_king[self.curr_player] = True

        self.register_enpassant(source_piece, source_bb, dest_bb)

        if (source_piece == Pieces.PAWN and
                ((self.curr_player == Colors.WHITE and
                    (dest_bb & mask_rank(Ranks.EIGHT))) or
                    (self.curr_player == Colors.BLACK and
                        (dest_bb & mask_rank(Ranks.ONE))))):
            self.pieces[self.curr_player][source_piece] ^= source_bb
            source_piece = promotion
        else:
            self.pieces[self.curr_player][source_piece] ^= source_bb
        self.pieces[self.curr_player][source_piece] |= dest_bb
        self._update_pieces()

    def apply_move_by_index(
            self, from_index: int, to_index: int, promotion: Pieces
                ):
        source = self.search_piece_by_mask((u64(1) << u64(from_index)))
        if not source:
            return MoveResult(False, False, None, None)
        dest = self.search_piece_by_mask((u64(1) << u64(to_index)))
        if not dest:
            dest = BBSearchResult(None, None, (u64(1) << u64(to_index)))
        self.apply_move(
            source.mask, source.piece,
            dest.mask, dest.piece, promotion=promotion
        )
        return MoveResult(True, bool(dest.piece), source, dest)

    def apply_move_to_board_str(self, fromfieldname: str, tofieldname: str):
        # TODO clean this up
        if source := self.search_piece_by_fieldname(
                fromfieldname, self.curr_player):
            dest = self.search_piece_by_fieldname(
                tofieldname, self.next_player)
            if not dest:
                # abusing the search result this way feels wrong
                # --
                # we don't need to check if one of our pieces is on the dest
                # square since that should be taken care of by the movegen
                dest = BBPieceSearchResult(
                        None, bb_from_fieldname(tofieldname), ''
                    )

            self.apply_move(source.bb, source.piece, dest.bb, dest.piece)
            return MoveResult(True, bool(dest.piece), source, dest)
        return MoveResult(False, False, None, None)

    def is_king_in_check(self, curr_player: Colors) -> u64:
        remember_player = self.curr_player
        self.curr_player = curr_player

        king_bb = self.pieces[self.next_player][Pieces.KING]
        check_attacks = u64(0)
        for pseudo in self.pseudo_movegen():
            if (pseudo.attack_bb & king_bb) != 0:
                # negate all non-check giving 'attack rays' e.g. for a rook,
                # only return the attack_bb in the direction that gives check
                if pseudo.piece.is_sliding():
                    for index in iter_bb_indexes(pseudo.attack_bb):
                        attack = self.pseudo_movegen_piece(pseudo.piece, index)
                        if attack and (attack.attack_bb & king_bb) == 0:
                            pseudo.attack_bb ^= attack.piece_bb
                    pseudo.attack_bb |= pseudo.piece_bb
                # only consider diagonal moves of pawns in the check calc as
                # pawns can't check a king head on
                # remove head on moves from attack_bb
                if pseudo.piece == Pieces.PAWN:
                    if self.curr_player == Colors.WHITE:
                        pseudo.attack_bb ^= (pseudo.piece_bb << u64(8))
                    elif self.curr_player == Colors.BLACK:
                        pseudo.attack_bb ^= (pseudo.piece_bb >> u64(8))
                # because we remove the forward north and south moves for pawns
                # attack_bb could be empty which whould throw a AttributeError
                # also a piece could be blocked on all sides e.g. a rook on h1
                # with pieces next to it
                if pseudo.attack_bb == EMPTY_BB:
                    continue
                check_attacks |= pseudo.attack_bb

        self.curr_player = remember_player
        return check_attacks

    def generate_checkmoves(self, active_check: u64):
        self.curr_player = self.next_player
        # check if the king can move away from the check condition
        # by either moving or taking a piece in his attack_bb
        king_moveset = self.get_pseudo_king_moves()
        for bb in iter_bb(king_moveset.attack_bb):
            bc = self._copy()
            take_piece = None
            if result := bc.search_piece_by_mask(bb):
                take_piece = result.piece

            bc.apply_move(
                king_moveset.piece_bb, Pieces.KING, bb, take_piece)
            in_check = bc.is_king_in_check(bc.next_player)
            if in_check != EMPTY_BB:
                king_moveset.attack_bb ^= bb

        # check if a piece can move into the active check to block it
        # and then see if the king is still in check if so, the move get's
        # deleted from the attack_bb. If in the end there is no attack_bb for
        # any piece and the king_moveset above is empty it's checkmate!
        blocking_moves = []
        for pseudo in self.pseudo_movegen():
            if pseudo.piece == Pieces.KING:
                continue

            for bb in iter_bb(pseudo.attack_bb):
                if (bb & active_check) == 0:
                    pseudo.attack_bb ^= bb

            # attack_bb now contains only moves that move into active_check.
            # now we need to see if by moving this piece we open up another
            # line that causes check. If that's the case we can't play the move
            if pseudo.attack_bb == EMPTY_BB:
                continue

            for bb in iter_bb(pseudo.attack_bb):
                bc = self._copy()
                take_piece = None
                if result := bc.search_piece_by_mask(bb):
                    take_piece = result.piece
                bc.apply_move(
                    pseudo.piece_bb, pseudo.piece, bb, take_piece
                )
                is_check = bc.is_king_in_check(bc.next_player)
                if is_check == EMPTY_BB:
                    blocking_moves.append(pseudo)

        if king_moveset.attack_bb != 0:
            blocking_moves.append(king_moveset)

        # return self.__gen_move_dict(blocking_moves)
        return {
            int(lsb_bitscan(x.piece_bb)):
            (lambda v: [
                int(x) for x in iter_bb_indexes(v)
            ])(x.attack_bb)
            for x in blocking_moves if x.attack_bb != EMPTY_BB
        }

    def try_move(
            self, from_index: int, to_index: int,
            promotion: Pieces = Pieces.QUEEN
            ) -> PlayerTurn:
        if from_index == to_index:
            return PlayerTurn(False, None, self.fen.generate())

        moves = self.legal_movegen()

        if from_index not in moves:
            return PlayerTurn(False, None, self.fen.generate())
        if to_index not in moves[from_index]:
            return PlayerTurn(False, None, self.fen.generate())

        board_copy = self._copy()
        result = board_copy.apply_move_by_index(
            from_index, to_index, promotion
        )
        if not result.valid:
            return PlayerTurn(False, None, self.fen.generate())

        flag = PlayerTurnFlag.NONE
        active_check = board_copy.is_king_in_check(self.curr_player)
        # print_u64_board(active_check)
        if active_check != 0:
            flag = PlayerTurnFlag.CHECK
            moves = board_copy.generate_checkmoves(active_check)
            if not moves:
                flag = PlayerTurnFlag.CHECKMATE
        else:
            board_copy.curr_player = board_copy.next_player
            moves = board_copy.legal_movegen()

        # TODO en passent

        self._insert(board_copy)

        if not self.not_king_has_moved():
            self.moved_king[self.curr_player] = True

        self.fen.set_board(self.pieces)
        if flag != PlayerTurnFlag.CHECKMATE:
            self.curr_player = self.next_player
            self.fen.set_side(self.curr_player)
            self.fen.set_castling(self.moved_king)

        return PlayerTurn(True, moves, self.fen.generate(), flag=flag)

    def try_move_fieldnames(
            self, fromfieldname: str, tofieldname: str,
            promotion: Union[Pieces, None] = None) -> PlayerTurn:

        from_index = fieldname_to_index(fromfieldname)
        to_index = fieldname_to_index(tofieldname)
        return self.try_move(from_index, to_index, promotion)
