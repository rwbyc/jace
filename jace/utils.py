from .consts import (
    Ranks, Files, RanksStrMap, FILES_CHARLIST,
    LSB_LOOKUP, MSB_LOOKUP, DEBRUJIN, EMPTY_BB, LSB_BB,
)

from io import TextIOWrapper
import sys
import numpy as np
from numpy import uint64 as u64, uint8 as u8
from typing import Union, List

COLOR = '\033[0;33m'
END = '\033[0m'


def u64tostr(number: u64, fill: int = 64) -> str:
    return bin(number)[2:].zfill(fill)


def print_u64(number: u64, fill: int = 64) -> None:
    print(u64tostr(number, fill))


def print_u64_board(
        number: u64, dots: bool = False,
        color: bool = True, io: TextIOWrapper = sys.stdout,
        print_letters: bool = False) -> None:

    line = u64tostr(number)
    strings = [line[i:i+8] for i in range(0, len(line), 8)]
    for i, char in enumerate(strings):
        if print_letters:
            io.write(f'{8-i}  ')
        for c in char[::-1]:
            if str(c) == '1':
                if color:
                    io.write(f'{COLOR}{c}{END}')
                else:
                    io.write(f'{c}')
            else:
                c = '.' if dots else c
                io.write(f'{c}')
        io.write('\n')
    if print_letters:
        io.write('   ABCDEFGH')
    io.write('\n')


def mask_rank(rank: Ranks) -> u64:
    return u64(0xff << 8 * rank.value)


def mask_file(file: Files) -> u64:
    return u64(0x0101010101010101 << file.value)


def popcount(bb: u64) -> u8:
    count = u8(0)
    while bb != EMPTY_BB:
        count = np.add(count, LSB_BB)
        bb &= np.subtract(bb, LSB_BB)
    return count


def bb_from_fieldname(fieldname: str) -> u64:
    """
    :param fieldname: human readable fieldname e.g. 'a1' or 'c4' or 'H7'
    :type fieldname: str
    :return: 64 bit bitboard where ` marks the piece the rest is 0
        for a piece on c3
        0000000000000000000000000000000000000000000001000000000000000000
        for a piece on h8
        1000000000000000000000000000000000000000000000000000000000000000
    :rtype: np.uint64
    """
    return u64(1) << u64(fieldname_to_index(fieldname))


def index_to_fieldname(index: Union[int, u8, u64]) -> str:
    row = int(index / 8)
    col = int(index % 8)
    return f'{FILES_CHARLIST[col]}{row + 1}'


def fieldname_to_index(fieldname: str) -> int:
    file = Files[fieldname[0].upper()]
    rank = Ranks[RanksStrMap[fieldname[1]]]
    return (rank * 8) + file


def lsb_bitscan(bb: u64) -> u8:
    return LSB_LOOKUP[np.multiply((bb & -bb), DEBRUJIN, dtype=u64) >> u8(58)]


def msb_bitscan(bb: u64) -> u8:
    bb |= bb >> u8(1)
    bb |= bb >> u8(2)
    bb |= bb >> u8(4)
    bb |= bb >> u8(8)
    bb |= bb >> u8(16)
    bb |= bb >> u8(32)
    return MSB_LOOKUP[np.multiply(bb, DEBRUJIN, dtype=u64) >> u8(58)]


def iter_bb_indexes(bb: u64) -> u8:
    """ yields lsb of bitboard and masks it until bitboard is exhausted
    :param bb: any bitboard
    :type bb: np.uint64
    :yield: index of lsb
    :rtype: np.uint8
    """
    while bb != EMPTY_BB:
        index = lsb_bitscan(bb)
        yield index
        bb ^= (u64(1) << index)


def iter_bb(bb: u64) -> u8:
    while bb != EMPTY_BB:
        index = lsb_bitscan(bb)
        rv = (u64(1) << index)
        yield rv
        bb ^= rv


def moves_as_fieldnames(moves: List['PseudoMove']) -> dict:
    """ takes in a list of PseudoMoves and generates a human readable
    Dictionary where the values and keys are fieldnames strings A1, H3, etc
    It will also remove the other king piece from the moveset

    :param moves: List of PseudoMoves
    :type moves: List[PseudoMove]
    :return: Dictionary {'A2': ['A3', 'A4'], ...}
    :rtype: dict
    """
    return {
        index_to_fieldname(k): (lambda v: [
            index_to_fieldname(x) for x in v
        ])(v) for k, v in moves.items()
    }