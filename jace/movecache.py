# from utils import print_u64_board
from . import movegen
from .classes import Directions
from .consts import (
    ROOK_SHIFTS, BISHOP_SHIFTS, Files, Ranks,
    ROOK_MAGIC, BISHOP_MAGIC
)
from .utils import mask_file, mask_rank, print_u64_board  # noqa

import numpy as np
from numpy import uint64 as u64


OUTER_MASK_INVERT = ~(mask_file(Files.A) | mask_file(Files.H) | mask_rank(Ranks.ONE) | mask_rank(Ranks.EIGHT))  # noqa


class MoveCache:
    def __init__(self, skip_init: bool = False):
        self.ROOK_ATT = np.array([], dtype=u64)
        self.ROOK_OFFSET = np.zeros(64, dtype=u64)
        self.ROOK_MAGIC = ROOK_MAGIC
        self.ROOK_MASK = np.zeros(64, dtype=u64)

        self.BISHOP_ATT = np.array([], dtype=u64)
        self.BISHOP_OFFSET = np.zeros(64, dtype=u64)
        self.BISHOP_MAGIC = BISHOP_MAGIC
        self.BISHOP_MASK = np.zeros(64, dtype=u64)

        self.KING = np.zeros(64, dtype=u64)
        self.KNIGHT = np.zeros(64, dtype=u64)

        if not skip_init:
            self.__generate_index_maps()
            self.initialize()

    def __generate_index_maps(self):
        #: This will calculate all the possible next_indexes for each direction
        #: for every index on the board.
        #: It will also create the masks for every index, excluding the edges
        #: Same goes for the BISHOP_INDEX_MAP
        self.ROOK_INDEX_MAP = {}
        for index in np.arange(64):
            data = {}
            mask_bb = u64(0)
            for direction in Directions.rook():
                next_index = direction.next_index(index)
                if next_index == 255:
                    continue

                data[direction] = []
                while next_index < 255:
                    mask_bb |= u64(1) << u64(next_index)
                    data[direction].append(next_index)
                    next_index = direction.next_index(next_index)

            row = int(index / 8)
            col = int(index % 8)

            #: This could be simplified but I cba
            if index == 0:
                mask_bb &= ~mask_file(Files.H)
                mask_bb &= ~mask_rank(Ranks.EIGHT)
            elif index == 7:
                mask_bb &= ~mask_file(Files.A)
                mask_bb &= ~mask_rank(Ranks.EIGHT)
            elif index == 56:
                mask_bb &= ~mask_file(Files.H)
                mask_bb &= ~mask_rank(Ranks.ONE)
            elif index == 63:
                mask_bb &= ~mask_file(Files.A)
                mask_bb &= ~mask_rank(Ranks.ONE)
            else:
                if row == 7:
                    mask_bb &= ~(
                        mask_file(Files.A)
                        | mask_file(Files.H)
                        | mask_rank(Ranks.ONE)
                    )
                elif row == 0:
                    mask_bb &= ~(
                        mask_file(Files.A)
                        | mask_file(Files.H)
                        | mask_rank(Ranks.EIGHT)
                    )
                elif col == 7:
                    mask_bb &= ~(
                        mask_file(Files.A)
                        | mask_rank(Ranks.ONE)
                        | mask_rank(Ranks.EIGHT)
                    )
                elif col == 0:
                    mask_bb &= ~(
                        mask_file(Files.H)
                        | mask_rank(Ranks.ONE)
                        | mask_rank(Ranks.EIGHT)
                    )
                else:
                    mask_bb &= OUTER_MASK_INVERT
            self.ROOK_MASK[index] = mask_bb
            if data:
                self.ROOK_INDEX_MAP[index] = data

        self.BISH_INDEX_MAP = {}
        for index in np.arange(64):
            data = {}
            mask_bb = u64(0)
            for direction in Directions.bishop():
                next_index = direction.next_index(index)
                if next_index == 255:
                    continue

                data[direction] = []
                while next_index < 255:
                    mask_bb |= u64(1) << u64(next_index)
                    data[direction].append(next_index)
                    next_index = direction.next_index(next_index)

            self.BISHOP_MASK[index] = (mask_bb & OUTER_MASK_INVERT)
            if data:
                self.BISH_INDEX_MAP[index] = data

    def initialize(self):
        for index in np.arange(64):
            # ROOK
            rook_occs = movegen.generate_all_occupancies(
                Directions.rook(), index
            )

            rook_atts = [
                movegen.generate_attack(index, x, self.ROOK_INDEX_MAP)
                for x in rook_occs
            ]

            # self.ROOK_OCC.extend(rook_occs)
            table = movegen.calc_att_table(
                index,
                rook_occs,
                rook_atts,
                ROOK_SHIFTS[index],
                True
            )
            #: Add the offset before appending the table
            self.ROOK_OFFSET[index] = len(self.ROOK_ATT)
            self.ROOK_ATT = np.append(self.ROOK_ATT, table, 0)

            # BISHOP
            bish_occs = movegen.generate_all_occupancies(
                Directions.bishop(), index)

            bish_atts = [
                movegen.generate_attack(index, x, self.BISH_INDEX_MAP)
                for x in bish_occs
            ]

            # self.BISHOP_OCC.extend(bish_occs)
            table = movegen.calc_att_table(
                index,
                bish_occs,
                bish_atts,
                BISHOP_SHIFTS[index],
                False
            )
            self.BISHOP_OFFSET[index] = len(self.BISHOP_ATT)
            self.BISHOP_ATT = np.append(self.BISHOP_ATT, table, 0)

            fake_bb = u64(1) << u64(index)
            self.KING[index] = movegen.compute_king_move(fake_bb)
            self.KNIGHT[index] = movegen.compute_knight_move(fake_bb)
