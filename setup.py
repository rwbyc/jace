import setuptools


setuptools.setup(
    name='jace',
    version='0.0.5',
    author='Tony',
    author_email='streubel.lzg@outlook.de',
    description='Just Another Chess Engine',
    install_requires=[
        'numpy>=1.21.4',
        'orjson>=3.6.4'
    ],
    packages=['jace'],
    python_requires='>=3.8',
)
