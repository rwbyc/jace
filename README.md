# JACE | Just Another Chess Engine

## Installing
```
python3 -m pip install -U -r requirements.txt
python3 setup.py install
```

## Running pytest
```
python3 -m pytest tests.py
```